# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/13 14:56:21 by lyhamrou          #+#    #+#              #
#    Updated: 2019/08/19 17:18:22 by lyhamrou         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fractol

CC = gcc

FLAGS = -Wall -Wextra -Werror

HEADER = includes/fractol.h

LDLIBS = -lft -L libft/ -lmlx -L minilibx_macos/ -framework OpenGL -framework AppKit

INCLUDES = -I includes/ -I libft/ -I minilibx_macos/

SRCS_NAME = main.c pannel.c key_event.c mouse_event.c loop_event.c init.c \
			fractol.c mandelbrot.c julia.c burningship.c \
			arrowdelbrot.c heart.c wave.c ugly.c wonder.c
SRCS_PATH = srcs/
SRCS = $(addprefix $(SRCS_PATH),$(SRCS_NAME))

OBJS_PATH = .objs/
OBJS_NAME = $(SRCS_NAME:.c=.o)
OBJS = $(addprefix $(OBJS_PATH),$(OBJS_NAME))

LIBFT_A = libft/libft.a

all: $(NAME)

$(NAME): create_obj_path $(LIBFT_A) $(OBJS)
	make -C minilibx_macos/
	$(CC) $(FLAGS) -o $(NAME) $(OBJS) $(LDLIBS) $(INCLUDES)

create_obj_path:
	mkdir -p .objs

$(LIBFT_A):
	make -C libft/

$(OBJS_PATH)%.o: $(SRCS_PATH)%.c $(HEADER)
	$(CC) $(FLAGS) $(INCLUDES) -o $@ -c $<

savetheworld: fclean
	git add .
	git commit -m "autosave"
	git push

mandelbrot: all
	./$(NAME) mandelbrot
	$(RM) $(NAME)

norm: fclean
	@echo "\n\nNORMINETTE LIBFT"
	norminette libft/
	@echo "\n\nNORMINETTE INCLUDES"
	norminette includes/
	@echo "\n\nNORMINETTE SRCS"
	norminette $(SRCS_PATH)

clean:
	$(RM) -rf $(OBJS_PATH)
	make clean -C libft/
	make clean -C minilibx_macos/

fclean: clean
	$(RM) $(NAME)
	make fclean -C libft/

re: fclean all

.PHONY: re fclean clean all $(NAME)
