/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 17:27:37 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/19 18:03:41 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <unistd.h>
# include <pthread.h>
# include <limits.h>
# include <stdlib.h>
# include <math.h>
# include "libft.h"
# include "mlx.h"

# define BLACK 0x0
# define WHITE 0xffffff
# define YELLOW 0xE9C92A
# define WIN_TITLE "FRACT'AULE"
# define MANDELBROT_X1 -2.1
# define MANDELBROT_X2 0.6
# define MANDELBROT_Y1 -1.2
# define MANDELBROT_Y2 1.2
# define JULIA_X1 -2
# define JULIA_X2 2
# define JULIA_Y1 -1.9
# define JULIA_Y2 1.9
# define BURNINGSHIP_X1 -2.2
# define BURNINGSHIP_X2 1.8
# define BURNINGSHIP_Y1 -2.5
# define BURNINGSHIP_Y2 1.5
# define ARROWDELBROT_X1 -0.39
# define ARROWDELBROT_X2 0.4
# define ARROWDELBROT_Y1 0.8
# define ARROWDELBROT_Y2 2
# define HEART_X1 -1.3
# define HEART_X2 1.4
# define HEART_Y1 -0.8
# define HEART_Y2 0.8
# define WAVE_X1 -1.3
# define WAVE_X2 1.4
# define WAVE_Y1 -0.8
# define WAVE_Y2 0.8
# define UGLY_X1 -1.45
# define UGLY_X2 1.5
# define UGLY_Y1 -1.2
# define UGLY_Y2 1.2
# define WONDER_X1 -2.1
# define WONDER_X2 2
# define WONDER_Y1 -2
# define WONDER_Y2 2.1
# define USAGE "Usage: ./fractol [fractal name]\n" FRACT_NAME FRACT_BONUS
# define FRACT_NAME "\n- mandelbrot\n- julia\n- burningship\n"
# define FRACT_BONUS "- arrowdelbrot\n- heart\n- wave\n- ugly\n- wonder"
# define INFO_TITLE "_______CONTROLS_______"
# define INFO_BORD "______________________"
# define INFO_FRACT "Fractal    :"
# define INFO_MOVE "MOVE       : arrows"
# define INFO_COLOR "COLOR      : c"
# define INFO_ESC "EXIT       : Esc"
# define INFO_ZOOM "ZOOM       : mouse clic"
# define INFO_SWITCH "SWITCH     : s"
# define INFO_STOP "STOP JULIA : space"
# define INFO_ITER "ITERATIONIONS  : u / i"
# define MOVE 0.2
# define ZOOM 1
# define ITERATION 50
# define PAS_ITERATION 10
# define MAX_ITERATION 250
# define MAX_ZOOM 30
# define KEY_ESC 53
# define KEY_SPACE 49
# define KEY_S 1
# define KEY_I 34
# define KEY_U 32
# define KEY_C 8
# define KEY_Q 12
# define KEY_W 13
# define KEY_R 15
# define KEY_LEFT 123
# define KEY_RIGHT 124
# define KEY_DOWN 125
# define KEY_UP 126
# define MOUSE_L 1
# define MOUSE_R 2
# define MOUSE_LL 4
# define MOUSE_RR 5
# define IMG_SIZE 900
# define WIN_X 1200
# define WIN_Y IMG_SIZE
# define IMG_X IMG_SIZE
# define IMG_Y IMG_SIZE

typedef enum	e_fract
{
	MANDELBROT,
	JULIA,
	BURNINGSHIP,
	ARROWDELBROT,
	HEART,
	WAVE,
	UGLY,
	WONDER,
}				t_fract;

typedef struct	s_env
{
	int			type;
	int			it;
	int			tmp;
	int			sl;
	int			bpp;
	int			edn;
	int			zoom;
	int			mouse_julia;
	int			it_sc;
	int			*data;
	int			*data2;
	int			*copy;
	void		*mlx;
	void		*win;
	void		*img;
	void		*img2;
	double		move;
	double		zr;
	double		zi;
	double		cr;
	double		ci;
	double		x1;
	double		x2;
	double		y1;
	double		y2;
	double		pasx;
	double		pasy;
	int			color[20];
}				t_env;

int				fractol(t_env *env);
void			init_mlx(t_env *env);
void			init_env(t_env *env);
int				init_copy(t_env *env, int i);
void			stock_color(t_env *env);
void			choose_fractal_init(t_env *env);
void			choose_fractal(t_env *env);

void			infos_pannel(t_env *e);
void			put_infos_status(t_env *env);

int				loop_event(int x, int y, t_env *env);
int				key_event(int key, t_env *env);
void			key_iteration(t_env *env, int key);
int				mouse_event(int but, int x, int y, t_env *env);

void			init_mandelbrot(t_env *env);
void			mandelbrot(t_env *env, int x);
void			init_julia(t_env *env);
void			julia(t_env *env, int x);
void			init_burningship(t_env *env);
void			burningship(t_env *env, int x);
void			init_arrowdelbrot(t_env *env);
void			arrowdelbrot(t_env *env, int i);
void			init_heart(t_env *env);
void			heart(t_env *env, int i);
void			init_wave(t_env *env);
void			wave(t_env *env, int i);
void			init_ugly(t_env *env);
void			ugly(t_env *env, int i);
void			init_wonder(t_env *env);
void			wonder(t_env *env, int i);

#endif
