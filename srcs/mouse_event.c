/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_event.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/14 13:23:31 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/19 16:51:42 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	mouse_zoom(t_env *env, int i, int x, int y)
{
	if (i == 0)
	{
		env->zoom++;
		env->move /= 2;
		env->pasx /= 2;
		env->pasy /= 2;
		env->x1 += x * env->pasx;
		env->x2 += x * env->pasx;
		env->y1 += y * env->pasy;
		env->y2 += y * env->pasy;
	}
	else
	{
		env->zoom--;
		env->move *= 2;
		env->x1 -= x * env->pasx;
		env->x2 -= x * env->pasx;
		env->y1 -= y * env->pasy;
		env->y2 -= y * env->pasy;
		env->pasx *= 2;
		env->pasy *= 2;
	}
}

int		mouse_event(int but, int x, int y, t_env *env)
{
	if ((but == MOUSE_L || but == MOUSE_RR) && env->zoom < MAX_ZOOM)
		mouse_zoom(env, 0, x, y);
	else if ((but == MOUSE_R || but == MOUSE_LL) && env->zoom > 0)
		mouse_zoom(env, 1, x, y);
	choose_fractal(env);
	return (1);
}
