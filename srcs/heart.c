/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heart.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/19 15:54:29 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/19 17:14:48 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		init_heart(t_env *env)
{
	env->type = HEART;
	env->x1 = HEART_X1;
	env->x2 = HEART_X2;
	env->y1 = HEART_Y1;
	env->y2 = HEART_Y2;
	env->pasx = (HEART_X2 - HEART_X1) / IMG_X;
	env->pasy = (HEART_Y2 - HEART_Y1) / IMG_Y;
	env->it = ITERATION;
	env->zoom = ZOOM;
	env->move = MOVE;
}

static int	iteration_heart(t_env *env, int x, int y)
{
	int		i;
	double	tmp;

	i = 0;
	env->zr = 0;
	env->zi = 0;
	env->cr = x * env->pasx + env->x1;
	env->ci = y * env->pasy + env->y1;
	while (env->zr * env->zr + env->zi * env->zi < 4 && i < env->it)
	{
		tmp = env->zr;
		env->zi = env->zr * env->zr - env->zi * env->zi + env->ci;
		env->zr = -1 * (3 * env->zi * tmp + env->cr);
		++i;
	}
	return (i);
}

void		heart(t_env *env, int i)
{
	int		x;
	int		y;
	int		it;

	y = 0;
	while (y < IMG_Y)
	{
		x = 0;
		while (x < IMG_X)
		{
			it = iteration_heart(env, x, y);
			if (it == env->it)
				env->data[i] = BLACK;
			else
				env->data[i] = env->color[it % 10];
			++i;
			++x;
		}
		++y;
	}
	put_infos_status(env);
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
}
