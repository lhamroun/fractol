/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 17:40:32 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/19 18:04:17 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

/*
**	__attribute__((destructor)) void function(void)
**	{while(1);}
*/

static int	check_argument(int ac, char **av)
{
	if (ac != 2)
		return (-1);
	if (ft_strcmp(av[1], "mandelbrot") == 0)
		return (MANDELBROT);
	else if (ft_strcmp(av[1], "julia") == 0)
		return (JULIA);
	else if (ft_strcmp(av[1], "burningship") == 0)
		return (BURNINGSHIP);
	else if (ft_strcmp(av[1], "arrowdelbrot") == 0)
		return (ARROWDELBROT);
	else if (ft_strcmp(av[1], "heart") == 0)
		return (HEART);
	else if (ft_strcmp(av[1], "wave") == 0)
		return (WAVE);
	else if (ft_strcmp(av[1], "cow") == 0)
		return (UGLY);
	else if (ft_strcmp(av[1], "wonder") == 0)
		return (WONDER);
	else
		return (-1);
}

int			main(int ac, char **av)
{
	t_env	env;

	if ((env.type = check_argument(ac, av)) == -1)
	{
		ft_putendl(USAGE);
		return (0);
	}
	if (fractol(&env) == 0)
		ft_putendl("Error malloc");
	return (0);
}
