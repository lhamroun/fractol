/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 22:01:03 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/19 17:16:04 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		init_mandelbrot(t_env *env)
{
	env->type = MANDELBROT;
	env->x1 = MANDELBROT_X1;
	env->x2 = MANDELBROT_X2;
	env->y1 = MANDELBROT_Y1;
	env->y2 = MANDELBROT_Y2;
	env->pasx = (MANDELBROT_X2 - MANDELBROT_X1) / IMG_X;
	env->pasy = (MANDELBROT_Y2 - MANDELBROT_Y1) / IMG_Y;
	env->it = ITERATION;
	env->zoom = ZOOM;
	env->move = MOVE;
}

static int	iteration_mandelbrot(t_env *env, int x, int y)
{
	int		i;
	double	tmp;

	i = 0;
	env->zr = 0;
	env->zi = 0;
	env->cr = x * env->pasx + env->x1;
	env->ci = y * env->pasy + env->y1;
	while (env->zr * env->zr + env->zi * env->zi < 4 && i < env->it)
	{
		tmp = env->zr;
		env->zr = env->zr * env->zr - env->zi * env->zi + env->cr;
		env->zi = 2 * env->zi * tmp + env->ci;
		++i;
	}
	return (i);
}

void		mandelbrot(t_env *env, int i)
{
	int		x;
	int		y;
	int		it;

	y = 0;
	while (y < IMG_Y)
	{
		x = 0;
		while (x < IMG_X)
		{
			it = iteration_mandelbrot(env, x, y);
			if (it == env->it)
				env->data[i] = BLACK;
			else
				env->data[i] = env->color[it % 20];
			++i;
			++x;
		}
		++y;
	}
	put_infos_status(env);
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
}
