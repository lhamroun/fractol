/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pannel.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/19 14:32:47 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/19 18:04:00 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		put_bordure(t_env *env, int x, int i)
{
	while (i < x)
	{
		mlx_pixel_put(env->mlx, env->win, IMG_X + i, 0, WHITE);
		mlx_pixel_put(env->mlx, env->win, IMG_X + i, 1, WHITE);
		mlx_pixel_put(env->mlx, env->win, IMG_X + i, 2, WHITE);
		mlx_pixel_put(env->mlx, env->win, IMG_X + i, IMG_Y - 3, WHITE);
		mlx_pixel_put(env->mlx, env->win, IMG_X + i, IMG_Y - 2, WHITE);
		mlx_pixel_put(env->mlx, env->win, IMG_X + i, IMG_Y - 1, WHITE);
		++i;
	}
	i = 0;
	while (i < WIN_Y)
	{
		mlx_pixel_put(env->mlx, env->win, IMG_X, i, WHITE);
		mlx_pixel_put(env->mlx, env->win, IMG_X + 1, i, WHITE);
		mlx_pixel_put(env->mlx, env->win, IMG_X + 2, i, WHITE);
		mlx_pixel_put(env->mlx, env->win, IMG_X + 3, i, WHITE);
		mlx_pixel_put(env->mlx, env->win, WIN_X - 3, i, WHITE);
		mlx_pixel_put(env->mlx, env->win, WIN_X - 2, i, WHITE);
		mlx_pixel_put(env->mlx, env->win, WIN_X - 1, i, WHITE);
		mlx_pixel_put(env->mlx, env->win, WIN_X - 4, i, WHITE);
		++i;
	}
}

static void	put_infos(t_env *e)
{
	mlx_string_put(e->mlx, e->win, IMG_X + 30, 20, WHITE, INFO_TITLE);
	mlx_string_put(e->mlx, e->win, IMG_X + 30, 50, WHITE, INFO_ESC);
	mlx_string_put(e->mlx, e->win, IMG_X + 30, 100, WHITE, INFO_MOVE);
	mlx_string_put(e->mlx, e->win, IMG_X + 30, 150, WHITE, INFO_ZOOM);
	mlx_string_put(e->mlx, e->win, IMG_X + 30, 200, WHITE, INFO_ITER);
	mlx_string_put(e->mlx, e->win, IMG_X + 30, 250, WHITE, INFO_COLOR);
	mlx_string_put(e->mlx, e->win, IMG_X + 30, 300, WHITE, INFO_SWITCH);
	mlx_string_put(e->mlx, e->win, IMG_X + 30, 350, WHITE, INFO_STOP);
	mlx_string_put(e->mlx, e->win, IMG_X + 30, 375, WHITE, INFO_BORD);
	mlx_string_put(e->mlx, e->win, IMG_X + 30, 500, WHITE, INFO_FRACT);
	mlx_string_put(e->mlx, e->win, IMG_X + 30, 550, WHITE, "zoom       :");
	mlx_string_put(e->mlx, e->win, IMG_X + 30, 600, WHITE, "Iterations :");
	mlx_string_put(e->mlx, e->win, IMG_X + 115, WIN_Y - 50, YELLOW, "Lyhamrou");
}

void		put_infos_status(t_env *e)
{
	char	*tmp;

	mlx_put_image_to_window(e->mlx, e->win, e->img2, IMG_X + 160, 480);
	tmp = ft_itoa(e->zoom);
	mlx_string_put(e->mlx, e->win, IMG_X + 160, 550, WHITE, tmp);
	free(tmp);
	tmp = ft_itoa(e->it);
	mlx_string_put(e->mlx, e->win, IMG_X + 160, 600, WHITE, tmp);
	if (e->type == MANDELBROT)
		mlx_string_put(e->mlx, e->win, IMG_X + 160, 500, WHITE, "MANDELBROT");
	else if (e->type == JULIA)
		mlx_string_put(e->mlx, e->win, IMG_X + 160, 500, WHITE, "JULIA");
	else if (e->type == BURNINGSHIP)
		mlx_string_put(e->mlx, e->win, IMG_X + 160, 500, WHITE, "BURNINGSHIP");
	else if (e->type == ARROWDELBROT)
		mlx_string_put(e->mlx, e->win, IMG_X + 160, 500, WHITE, "ARROWDELBROT");
	else if (e->type == HEART)
		mlx_string_put(e->mlx, e->win, IMG_X + 160, 500, WHITE, "HEART");
	else if (e->type == WAVE)
		mlx_string_put(e->mlx, e->win, IMG_X + 160, 500, WHITE, "WAVE");
	else if (e->type == UGLY)
		mlx_string_put(e->mlx, e->win, IMG_X + 160, 500, WHITE, "COW");
	else if (e->type == WONDER)
		mlx_string_put(e->mlx, e->win, IMG_X + 160, 500, WHITE, "WONDER");
	free(tmp);
}

void		infos_pannel(t_env *env)
{
	int		x;

	x = WIN_X - IMG_X;
	put_bordure(env, x, 0);
	put_infos(env);
}
