/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   burningship.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 17:35:06 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/19 17:14:42 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		init_burningship(t_env *env)
{
	env->type = BURNINGSHIP;
	env->x1 = BURNINGSHIP_X1;
	env->x2 = BURNINGSHIP_X2;
	env->y1 = BURNINGSHIP_Y1;
	env->y2 = BURNINGSHIP_Y2;
	env->pasx = (BURNINGSHIP_X2 - BURNINGSHIP_X1) / IMG_X;
	env->pasy = (BURNINGSHIP_Y2 - BURNINGSHIP_Y1) / IMG_Y;
	env->it = ITERATION;
	env->zoom = ZOOM;
	env->move = MOVE;
}

static int	iteration_burningship(t_env *env, int x, int y)
{
	int		i;
	double	tmp;

	i = 0;
	env->zr = 0;
	env->zi = 0;
	env->cr = x * env->pasx + env->x1;
	env->ci = y * env->pasy + env->y1;
	while (env->zr * env->zr + env->zi * env->zi < 4 && i < env->it)
	{
		tmp = env->zr;
		env->zr = env->zr * env->zr - env->zi * env->zi + env->cr;
		env->zi = ft_abs_d(2 * env->zi * tmp) + env->ci;
		++i;
	}
	return (i);
}

void		burningship(t_env *env, int i)
{
	int		x;
	int		y;
	int		it;

	y = 0;
	while (y < IMG_Y)
	{
		x = 0;
		while (x < IMG_X)
		{
			it = iteration_burningship(env, x, y);
			if (it == env->it)
				env->data[i] = BLACK;
			else
				env->data[i] = env->color[it % 20];
			++i;
			++x;
		}
		++y;
	}
	put_infos_status(env);
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
}
