/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 22:01:34 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/19 17:15:12 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		init_julia(t_env *env)
{
	env->type = JULIA;
	env->x1 = JULIA_X1;
	env->x2 = JULIA_X2;
	env->y1 = JULIA_Y1;
	env->y2 = JULIA_Y2;
	env->pasx = (JULIA_X2 + ft_abs_d(JULIA_X1)) / IMG_X;
	env->pasy = (JULIA_Y2 + ft_abs_d(JULIA_Y1)) / IMG_Y;
	env->it = ITERATION;
	env->zoom = ZOOM;
	env->move = MOVE;
	env->cr = 0.285;
	env->ci = 0.01;
	env->mouse_julia = 1;
}

static int	iteration_julia(t_env *env, int x, int y)
{
	int		i;
	double	tmp;

	i = 0;
	env->zr = x * env->pasx + env->x1;
	env->zi = y * env->pasy + env->y1;
	while (env->zr * env->zr + env->zi * env->zi < 4 && i < env->it)
	{
		tmp = env->zr;
		env->zr = env->zr * env->zr - env->zi * env->zi + env->cr;
		env->zi = 2 * env->zi * tmp + env->ci;
		++i;
	}
	return (i);
}

void		julia(t_env *env, int i)
{
	int		x;
	int		y;
	int		it;

	y = 0;
	while (y < IMG_Y)
	{
		x = 0;
		while (x < IMG_X)
		{
			it = iteration_julia(env, x, y);
			if (it == env->it)
				env->data[i] = BLACK;
			else
				env->data[i] = env->color[it % 20];
			++i;
			++x;
		}
		++y;
	}
	put_infos_status(env);
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
}
