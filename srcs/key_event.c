/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_event.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 17:45:09 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/19 16:51:36 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	key_color(t_env *env)
{
	int		i;

	i = 0;
	if (env->color[0] > 9000)
	{
		while (i < 10)
		{
			env->color[i] = env->color[i] >> 1;
			++i;
		}
	}
	else
		stock_color(env);
}

void	key_reset(t_env *env)
{
	int		i;

	i = 0;
	while (i < IMG_X * IMG_Y)
	{
		env->data[i] = env->copy[i];
		++i;
	}
	stock_color(env);
	choose_fractal_init(env);
}

void	key_move(t_env *env, int key)
{
	if (key == KEY_DOWN)
	{
		env->y1 -= env->move;
		env->y2 -= env->move;
	}
	else if (key == KEY_UP)
	{
		env->y1 += env->move;
		env->y2 += env->move;
	}
	else if (key == KEY_RIGHT)
	{
		env->x1 -= env->move;
		env->x2 -= env->move;
	}
	else if (key == KEY_LEFT)
	{
		env->x1 += env->move;
		env->x2 += env->move;
	}
}

void	key_switch(t_env *env)
{
	if (env->type == MANDELBROT)
		init_julia(env);
	else if (env->type == JULIA)
		init_burningship(env);
	else if (env->type == BURNINGSHIP)
		init_arrowdelbrot(env);
	else if (env->type == ARROWDELBROT)
		init_heart(env);
	else if (env->type == HEART)
		init_wave(env);
	else if (env->type == WAVE)
		init_ugly(env);
	else if (env->type == UGLY)
		init_wonder(env);
	else if (env->type == WONDER)
		init_mandelbrot(env);
}

int		key_event(int key, t_env *env)
{
	if (key == 53)
		exit(0);
	else if (key == KEY_I || key == KEY_U)
		key_iteration(env, key);
	else if (key == KEY_C)
		key_color(env);
	else if (key == KEY_R)
		key_reset(env);
	else if (key == KEY_S)
		key_switch(env);
	else if (key == KEY_SPACE)
		env->mouse_julia = 0;
	else if (key == KEY_UP || key == KEY_DOWN
		|| key == KEY_LEFT || key == KEY_RIGHT)
		key_move(env, key);
	choose_fractal(env);
	return (1);
}
