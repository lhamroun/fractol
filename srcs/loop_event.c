/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop_event.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 17:35:20 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/19 16:53:50 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		loop_event(int x, int y, t_env *env)
{
	if (env->type == JULIA && env->mouse_julia == 1)
	{
		env->cr = x * env->pasx + env->x1;
		env->ci = y * env->pasy + env->y1;
		julia(env, 0);
	}
	return (1);
}
