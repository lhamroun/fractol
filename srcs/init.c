/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 17:49:36 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/19 16:32:24 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	key_iteration(t_env *env, int key)
{
	if (key == KEY_I && env->it < MAX_ITERATION)
		env->it += PAS_ITERATION;
	else if (key == KEY_U && env->it > 10)
		env->it -= PAS_ITERATION;
}

int		init_copy(t_env *env, int i)
{
	if (!(env->copy = (int *)ft_memalloc(sizeof(int) * (IMG_X * IMG_Y))))
		return (0);
	while (i < IMG_X * IMG_Y)
	{
		env->copy[i] = env->data[i];
		++i;
	}
	return (1);
}

void	stock_color(t_env *env)
{
	env->color[0] = 0x0;
	env->color[1] = 0x010004;
	env->color[2] = 0x04000F;
	env->color[3] = 0x07001A;
	env->color[4] = 0x0E0132;
	env->color[5] = 0x100039;
	env->color[6] = 0x150147;
	env->color[7] = 0x180151;
	env->color[8] = 0x1B015C;
	env->color[9] = 0x1D0164;
	env->color[10] = 0x20016D;
	env->color[11] = 0x230178;
	env->color[12] = 0x260183;
	env->color[13] = 0x29028D;
	env->color[14] = 0x2A0195;
	env->color[15] = 0x2C019E;
	env->color[16] = 0x2E01A6;
	env->color[17] = 0x0601A9;
	env->color[18] = 0x0702AF;
	env->color[19] = 0x0702C1;
}

void	init_env(t_env *env)
{
	env->it = ITERATION;
	env->zoom = ZOOM;
	env->move = MOVE;
	stock_color(env);
	choose_fractal_init(env);
}

void	init_mlx(t_env *e)
{
	e->mlx = mlx_init();
	e->win = mlx_new_window(e->mlx, WIN_X, WIN_Y, WIN_TITLE);
	e->img = mlx_new_image(e->mlx, IMG_X, IMG_Y);
	e->data = (int *)mlx_get_data_addr(e->img, &e->bpp, &e->sl, &e->edn);
	ft_memset(e->data, 0, sizeof(int) * IMG_X * IMG_Y);
	e->img2 = mlx_new_image(e->mlx, 120, 200);
	e->data2 = (int *)mlx_get_data_addr(e->img2, &e->bpp, &e->sl, &e->edn);
	ft_memset(e->data2, 0, sizeof(int) * 120 * 200);
}
