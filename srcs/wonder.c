/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wonder.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/19 17:16:18 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/19 17:16:21 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		init_wonder(t_env *env)
{
	env->type = WONDER;
	env->x1 = WONDER_X1;
	env->x2 = WONDER_X2;
	env->y1 = WONDER_Y1;
	env->y2 = WONDER_Y2;
	env->pasx = (WONDER_X2 - WONDER_X1) / IMG_X;
	env->pasy = (WONDER_Y2 - WONDER_Y1) / IMG_Y;
	env->it = ITERATION;
	env->zoom = ZOOM;
	env->move = MOVE;
}

static int	iteration_wonder(t_env *env, int x, int y)
{
	int		i;
	double	tmp;

	i = 0;
	env->cr = 0;
	env->ci = 0;
	env->zr = x * env->pasx + env->x1;
	env->zi = y * env->pasy + env->y1;
	while (env->zr * env->zr + env->zi * env->zi < 4 && i < env->it)
	{
		tmp = env->zr;
		env->zi = env->zr * env->zr - env->zi * env->zi + env->cr;
		env->zr = 2 * env->zi * tmp + env->ci;
		++i;
	}
	return (i);
}

void		wonder(t_env *env, int i)
{
	int		x;
	int		y;
	int		it;

	y = 0;
	while (y < IMG_Y)
	{
		x = 0;
		while (x < IMG_X)
		{
			it = iteration_wonder(env, x, y);
			if (it == env->it)
				env->data[i] = BLACK;
			else
				env->data[i] = env->color[it % 10];
			++i;
			++x;
		}
		++y;
	}
	put_infos_status(env);
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
}
