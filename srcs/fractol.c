/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 17:49:09 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/19 16:49:13 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	choose_fractal_init(t_env *env)
{
	if (env->type == MANDELBROT)
		init_mandelbrot(env);
	else if (env->type == JULIA)
		init_julia(env);
	else if (env->type == BURNINGSHIP)
		init_burningship(env);
	else if (env->type == ARROWDELBROT)
		init_arrowdelbrot(env);
	else if (env->type == HEART)
		init_heart(env);
	else if (env->type == WAVE)
		init_wave(env);
	else if (env->type == UGLY)
		init_ugly(env);
	else if (env->type == WONDER)
		init_wonder(env);
}

void	choose_fractal(t_env *env)
{
	if (env->type == MANDELBROT)
		mandelbrot(env, 0);
	else if (env->type == JULIA)
		julia(env, 0);
	else if (env->type == BURNINGSHIP)
		burningship(env, 0);
	else if (env->type == ARROWDELBROT)
		arrowdelbrot(env, 0);
	else if (env->type == HEART)
		heart(env, 0);
	else if (env->type == WAVE)
		wave(env, 0);
	else if (env->type == UGLY)
		ugly(env, 0);
	else if (env->type == WONDER)
		wonder(env, 0);
}

int		fractol(t_env *env)
{
	init_mlx(env);
	init_env(env);
	infos_pannel(env);
	choose_fractal(env);
	if (init_copy(env, 0) == 0)
		return (0);
	if (mlx_hook(env->win, 6, 1L < 6, loop_event, env) == 0)
		return (0);
	if (mlx_key_hook(env->win, key_event, env) == 0)
		return (0);
	if (mlx_mouse_hook(env->win, mouse_event, env) == 0)
		return (0);
	mlx_loop(env->mlx);
	return (1);
}
